package ru.yaleksandrova.tm.api;

import ru.yaleksandrova.tm.model.AbstractEntity;
import ru.yaleksandrova.tm.repository.AbstractRepository;

public interface IService<E extends AbstractEntity> extends IRepository<E>  {

}
