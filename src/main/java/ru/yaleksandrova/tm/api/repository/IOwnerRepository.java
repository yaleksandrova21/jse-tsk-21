package ru.yaleksandrova.tm.api.repository;

import ru.yaleksandrova.tm.api.IRepository;
import ru.yaleksandrova.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    void add(E entity);

    void remove(String userId, E entity);

    void clear(String userId);

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    E findById(String userId, String id);

    E findByIndex(String userId, Integer index);

    E removeById(String userId, String id);

    E removeByIndex(String userId, Integer index);

    Integer size(String userId);

}

