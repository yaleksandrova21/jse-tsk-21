package ru.yaleksandrova.tm.api.repository;

import ru.yaleksandrova.tm.api.IRepository;
import ru.yaleksandrova.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    User removeUserById(String id);

    User removeUserByLogin(String login);

}
