package ru.yaleksandrova.tm.command.user;

import ru.yaleksandrova.tm.command.AbstractCommand;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public class UserCreateCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-create";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create new user";
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN: ");
        final String login = ApplicationUtil.nextLine();
        System.out.println("ENTER PASSWORD: ");
        final String password = ApplicationUtil.nextLine();
        System.out.println("ENTER EMAIL: ");
        final String email = ApplicationUtil.nextLine();
        serviceLocator.getUserService().create(login, password, email);
    }

}
