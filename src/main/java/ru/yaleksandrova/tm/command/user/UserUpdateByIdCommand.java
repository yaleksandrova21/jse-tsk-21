package ru.yaleksandrova.tm.command.user;

import ru.yaleksandrova.tm.command.AbstractUserCommand;
import ru.yaleksandrova.tm.model.User;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public class UserUpdateByIdCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-update-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update user profile";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROFILE]");
        System.out.println("ENTER USER ID: ");
        final String id = ApplicationUtil.nextLine();
        final User user = serviceLocator.getUserService().findById(id);
        System.out.println("ENTER FIRST NAME:");
        final String firstName = ApplicationUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        final String lastName = ApplicationUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        final String middleName = ApplicationUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = ApplicationUtil.nextLine();
        serviceLocator.getUserService().updateUser(id, firstName, lastName, middleName, email);
        System.out.println("User profile updated!");
    }

}
